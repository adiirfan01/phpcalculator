<?php

namespace Jakmall\Recruitment\Calculator\Tests\Helpers;


use Jakmall\Recruitment\Calculator\Enum\DriverEnum;

trait StorageTrait
{
    /**
     * Insert Data to file base on driver
     *
     * @param array      $array
     * @param DriverEnum $driverEnum
     */
    public function insert(array $array, DriverEnum $driverEnum)
    {
        $fileLog = __DIR__."/../../../storage/".getenv('FILE_STORAGE');
        $latestLog = __DIR__."/../../../storage/".getenv('LATEST_STORAGE');

        $json = json_encode($array, JSON_PRETTY_PRINT);
        if(DriverEnum::latest()->isEqual($driverEnum)) {
            file_put_contents($latestLog, $json);
        }
        elseif(DriverEnum::latest()->isEqual($driverEnum)) {
            file_put_contents($fileLog, $json);
        }
        else
        {
            file_put_contents($latestLog, $json);
            file_put_contents($fileLog, $json);
        }
    }

    /**
     * Read Data from storage base on driver
     *
     * @param  DriverEnum $driverEnum
     * @return array
     */
    public function read(DriverEnum $driverEnum) : array
    {
        $fileLog = __DIR__."/../../../storage/".getenv('FILE_STORAGE');
        $latestLog = __DIR__."/../../../storage/".getenv('LATEST_STORAGE');

        if(DriverEnum::latest()->isEqual($driverEnum)) {
            $data = file_get_contents($latestLog);
        }
        elseif(DriverEnum::latest()->isEqual($driverEnum)) {
            $data = file_get_contents($fileLog);
        }
        else
        {
            $data = file_get_contents($fileLog);
        }

        $data = json_decode($data, true);
        return $data;
    }

    /**
     * Read data from storage based on id and driver
     *
     * @param  string     $id
     * @param  DriverEnum $driverEnum
     * @return array
     */
    public function readById(string $id, DriverEnum $driverEnum) : array
    {
        $data = $this->read($driverEnum);
        $collection = collect($data);
        $result = $collection->where('id', $id)
            ->first();
        return $result;
    }


}
