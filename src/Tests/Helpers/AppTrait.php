<?php

namespace Jakmall\Recruitment\Calculator\Tests\Helpers;

use Dotenv\Dotenv;
use Illuminate\Console\Application;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Symfony\Component\Process\Process;

trait AppTrait
{
    private static $process;
    /**
     * Add Command to Application
     *
     * @return Application
     */
    public function makeCommands() : Application
    {
        $dotenv = Dotenv::createImmutable(__DIR__.'/../../../', '.env.testing');
        $dotenv->load();

        $container = new Container();
        $dispatcher = new Dispatcher();
        $app = new Application($container, $dispatcher, '1.0.1');

        $appConfig = include __DIR__.'/../../../config/app.php';
        $providers = $appConfig['providers'];

        foreach ($providers as $provider) {
            $container->make($provider)->register($container);
        }

        $commands = include __DIR__.'/../../../commands.php';
        $commands = collect($commands)
            ->map(
                function ($command) use ($app) {
                    return $app->getLaravel()->make($command);
                }
            )
            ->all();
        $app->addCommands($commands);
        return $app;
    }

    public static function makeServer()
    {
        self::$process = new Process("php -S 0.0.0.0:9191 public/index.php");
        self::$process->start();

        usleep(500000);
    }

    public static function stopServer()
    {
        self::$process->stop();
    }
}
