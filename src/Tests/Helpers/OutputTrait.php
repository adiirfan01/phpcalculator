<?php

namespace Jakmall\Recruitment\Calculator\Tests\Helpers;

trait OutputTrait
{
    /**
     * Save command result to folder output
     *
     * @param $command
     * @param $response
     */
    public function saveCommandResponse($command, $response)
    {
        $this->saveFile($this->getFilePath($command), $response);
    }

    /**
     * Get saved command response
     *
     * @param  $command
     * @return false|string
     */
    public function getCommandResponse($command)
    {
        $path = $this->getFilePath($command);
        $content = @file_get_contents($path);

        return $content;
    }

    private function saveFile($path, $response)
    {
        $dir = dirname($path);
        @mkdir($dir, 0777, true);
        file_put_contents($path, $response);
    }

    private function getFilePath($command)
    {
        $path = __DIR__ . '/../output/';
        return $path . '/' . $command . '.log';
    }
}
