<?php

namespace Jakmall\Recruitment\Calculator\Tests;

use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Tests\Helpers\StorageTrait;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    use StorageTrait;

    /**
     * clear file after test
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $this->insert([], DriverEnum::composite());
    }
}
