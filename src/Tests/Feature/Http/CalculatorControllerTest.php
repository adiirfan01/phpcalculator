<?php

namespace Jakmall\Recruitment\Calculator\Tests\Feature\Http;

use Dotenv\Dotenv;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Tests\BaseTestCase;
use Jakmall\Recruitment\Calculator\Tests\Helpers\AppTrait;
use GuzzleHttp\Client;

class CalculatorControllerTest extends BaseTestCase
{
    use AppTrait;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $dotenv = Dotenv::createImmutable(__DIR__ . '/../../../../', '.env.testing');
        $dotenv->load();

        self::makeServer();
    }

    public static function tearDownAfterClass(): void
    {
        self::stopServer();
    }

    public function testCalculatorAdd()
    {
        $client = new Client(['http_errors' => false]);

        $response = $client->request(
            "POST", "http://localhost:9191/calculator/add", [
            'form_params' => [
                "input" => [1, 1]
            ]
            ]
        );

        $responseBody = json_decode($response->getBody(), true);
        $this->assertEquals(
            [
            "command" => "add",
            "operation" => "1 + 1",
            "result" => 2,
            ], $responseBody
        );
        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testCalculatorSubstract()
    {
        $client = new Client(['http_errors' => false]);

        $response = $client->request(
            "POST", "http://localhost:9191/calculator/substract", [
            'form_params' => [
                "input" => [2, 1]
            ]
            ]
        );

        $responseBody = json_decode($response->getBody(), true);
        $this->assertEquals(
            [
            "command" => "substract",
            "operation" => "2 - 1",
            "result" => 1,
            ], $responseBody
        );
        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testCalculatorPower()
    {
        $client = new Client(['http_errors' => false]);

        $response = $client->request(
            "POST", "http://localhost:9191/calculator/power", [
            'form_params' => [
                "input" => [2, 2]
            ]
            ]
        );

        $responseBody = json_decode($response->getBody(), true);
        $this->assertEquals(
            [
            "command" => "power",
            "operation" => "2 ^ 2",
            "result" => 4,
            ], $responseBody
        );
        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testCalculatorMultiply()
    {
        $client = new Client(['http_errors' => false]);

        $response = $client->request(
            "POST", "http://localhost:9191/calculator/multiply", [
            'form_params' => [
                "input" => [2, 2]
            ]
            ]
        );

        $responseBody = json_decode($response->getBody(), true);
        $this->assertEquals(
            [
            "command" => "multiply",
            "operation" => "2 * 2",
            "result" => 4,
            ], $responseBody
        );
        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testCalculatorDivide()
    {
        $client = new Client(['http_errors' => false]);

        $response = $client->request(
            "POST", "http://localhost:9191/calculator/divide", [
            'form_params' => [
                "input" => [2, 2]
            ]
            ]
        );

        $responseBody = json_decode($response->getBody(), true);
        $this->assertEquals(
            [
            "command" => "divide",
            "operation" => "2 / 2",
            "result" => 1,
            ], $responseBody
        );
        $this->assertEquals(201, $response->getStatusCode());
    }
}
