<?php

namespace Jakmall\Recruitment\Calculator\Tests\Feature\Http;

use Dotenv\Dotenv;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Tests\BaseTestCase;
use Jakmall\Recruitment\Calculator\Tests\Helpers\AppTrait;
use GuzzleHttp\Client;

class HistoryControllerTest extends BaseTestCase
{
    use AppTrait;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $dotenv = Dotenv::createImmutable(__DIR__.'/../../../../', '.env.testing');
        $dotenv->load();

        self::makeServer();
    }

    public static function tearDownAfterClass(): void
    {
        self::stopServer();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->generateData();
    }

    public function testGetAllHistory()
    {
        $client = new Client(['http_errors' => false]);
        $response = $client->request("GET", "http://localhost:9191/calculator/");
        $expected = [
            [
                "id" => "1",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "input" => [1,1]
            ],
            [
                "id" => "2",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "input" => [1,1]
            ],
            [
                "id" => "3",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "input" => [1,1]
            ]
        ];

        $this->assertEquals($expected, json_decode($response->getBody(), true));
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetHistoryById()
    {
        $client = new Client(['http_errors' => false]);
        $response = $client->request("GET", "http://localhost:9191/calculator/1");
        $expected = [
            "id" => "1",
            "command" => "Add",
            "operation" => "1 + 1",
            "result" => 2,
            "input" => [1,1]
        ];

        $this->assertEquals($expected, json_decode($response->getBody(), true));
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDeleteHistory()
    {
        $client = new Client(['http_errors' => false]);
        $response = $client->request("DELETE", "http://localhost:9191/calculator/2");
        $expected = [
            [
                "id" => "1",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ],
            [
                "id" => "3",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ]
        ];
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals($expected, $this->read(DriverEnum::composite()));
    }

    private function generateData()
    {
        $data = [
            [
                "id" => "1",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ],
            [
                "id" => "2",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ],
            [
                "id" => "3",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ]
        ];
        $this->insert($data, DriverEnum::composite());
    }
}
