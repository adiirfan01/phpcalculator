<?php

namespace Jakmall\Recruitment\Calculator\Tests\Feature\Commands;

use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Tests\BaseTestCase;
use Jakmall\Recruitment\Calculator\Tests\Helpers\AppTrait;
use Symfony\Component\Console\Tester\CommandTester;

class HistoryClearTest extends BaseTestCase
{
    protected $commandTester;

    use AppTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->app = $this->makeCommands();
        $this->generateData();
    }

    public function testClearAll()
    {
        $this->generateData();
        $commands = $this->app->find('history:clear');
        $this->commandTester = new CommandTester($commands);
        $this->commandTester->execute([]);

        $dataLatest = $this->read(DriverEnum::latest());
        $dataFile = $this->read(DriverEnum::file());
        $this->assertEquals([], $dataLatest);
        $this->assertEquals([], $dataFile);
    }

    public function testClearById()
    {
        $this->generateData();
        $commands = $this->app->find('history:clear');
        $this->commandTester = new CommandTester($commands);
        $this->commandTester->execute(
            [
            'id' => [2]
            ]
        );

        $expected = [
            [
                "id" => "1",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ],
            [
                "id" => "3",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ]
        ];

        $dataLatest = $this->read(DriverEnum::latest());
        $dataFile = $this->read(DriverEnum::file());
        $this->assertEquals($expected, $dataLatest);
        $this->assertEquals($expected, $dataFile);
    }

    private function generateData()
    {
        $data = [
            [
                "id" => "1",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ],
            [
                "id" => "2",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ],
            [
                "id" => "3",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ]
        ];
        $this->insert($data, DriverEnum::composite());
    }
}
