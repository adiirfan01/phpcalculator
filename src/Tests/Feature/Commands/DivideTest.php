<?php

namespace Jakmall\Recruitment\Calculator\Tests\Feature\Commands;

use Carbon\Carbon;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Tests\BaseTestCase;
use Jakmall\Recruitment\Calculator\Tests\Helpers\AppTrait;
use Symfony\Component\Console\Tester\CommandTester;

class DivideTest extends BaseTestCase
{
    protected $commandTester;

    use AppTrait;

    public function setUp(): void
    {
        parent::setUp();
        Carbon::setTestNow('2021-09-13 03:04:05');
        $commands = $this->makeCommands()->find('divide');
        $this->commandTester = new CommandTester($commands);
    }

    public function testDivide()
    {
        $this->commandTester->execute(
            [
            'numbers' => [4, 2, 1]
            ]
        );

        $result = str_replace(array("\r", "\n"), '', $this->commandTester->getDisplay());
        $this->assertEquals('4 / 2 / 1 = 2', $result);
        $data = $this->read(DriverEnum::composite());

        $this->assertEquals(
            [
            'id' => $data[0]['id'],
            "command" => "Divide",
            "operation" => "4 / 2 / 1",
            "result" => 2,
            "last_updated" => Carbon::now()->toDateTimeString()
            ], $data[0]
        );
    }

    public function testDivideWithAlphabet()
    {
        try{
            $this->commandTester->execute(
                [
                'numbers' => [4, 2, 'A']
                ]
            );
        } catch (\Throwable $th){
            $this->assertEquals('this input "A" is not number', $th->getMessage());
        }
    }

    public function testDivideWithZero()
    {
        try{
            $this->commandTester->execute(
                [
                'numbers' => [4, 0]
                ]
            );
        } catch (\Throwable $th){
            $this->assertEquals('Error Division by Zero', $th->getMessage());
        }
    }
}
