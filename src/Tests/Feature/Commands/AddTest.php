<?php

namespace Jakmall\Recruitment\Calculator\Tests\Feature\Commands;

use Carbon\Carbon;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Tests\BaseTestCase;
use Jakmall\Recruitment\Calculator\Tests\Helpers\AppTrait;
use Symfony\Component\Console\Tester\CommandTester;

class AddTest extends BaseTestCase
{
    protected $commandTester;

    use AppTrait;

    public function setUp(): void
    {
        parent::setUp();
        Carbon::setTestNow('2021-09-13 03:04:05');
        $commands = $this->makeCommands()->find('add');
        $this->commandTester = new CommandTester($commands);
    }

    public function testAdd()
    {
        $this->commandTester->execute(
            [
            'numbers' => [1,2,3]
            ]
        );

        $result = str_replace(array("\r", "\n"), '', $this->commandTester->getDisplay());
        $this->assertEquals('1 + 2 + 3 = 6', $result);

        $data = $this->read(DriverEnum::composite());
        $this->assertEquals(
            [
            'id' => $data[0]['id'],
            "command" => "Add",
            "operation" => "1 + 2 + 3",
            "result" => 6,
            "last_updated" => Carbon::now()->toDateTimeString()
            ], $data[0]
        );
    }

    public function testAddWithAlphabet()
    {
        try{
            $this->commandTester->execute(
                [
                'numbers' => [1,2,'A']
                ]
            );
        } catch (\Throwable $th){
            $this->assertEquals('this input "A" is not number', $th->getMessage());
        }
    }
}
