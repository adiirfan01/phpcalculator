<?php

namespace Jakmall\Recruitment\Calculator\Tests\Feature\Commands;

use Carbon\Carbon;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Tests\BaseTestCase;
use Jakmall\Recruitment\Calculator\Tests\Helpers\AppTrait;
use Jakmall\Recruitment\Calculator\Tests\Helpers\OutputTrait;
use Symfony\Component\Console\Tester\CommandTester;

class HistoryListTest extends BaseTestCase
{
    const GENERATE_RESPONSE = false; // NOTE : change it to true if output folder is empty / data changed

    protected $commandTester;

    protected $app;

    use AppTrait;
    use OutputTrait;

    public function setUp(): void
    {
        parent::setUp();
        $commands = $this->makeCommands()->find('history:list');
        $this->commandTester = new CommandTester($commands);
        $this->generateData();
    }

    public function testHistoryListAll()
    {
        $this->commandTester->execute([]);

        if(self::GENERATE_RESPONSE) {
            $this->saveCommandResponse('historyListAll', $this->commandTester->getDisplay());
        }

        $expected = $this->getCommandResponse('historyListAll');
        $this->assertEquals($expected, $this->commandTester->getDisplay());
    }

    public function testHistoryListWithId()
    {
        $this->generateData();
        Carbon::setTestNow('2021-09-13 03:04:05');
        $this->commandTester->execute(
            [
            'id' => [1]
            ]
        );

        if(self::GENERATE_RESPONSE) {
            $this->saveCommandResponse('historyListWithId', $this->commandTester->getDisplay());
        }

        $data = $this->readById('1', DriverEnum::latest());
        $this->assertEquals($data['last_updated'], Carbon::now()->toDateTimeString());

        $expected = $this->getCommandResponse('historyListWithId');
        $this->assertEquals($expected, $this->commandTester->getDisplay());
    }

    private function generateData()
    {
        $data = [
            [
                "id" => "1",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ],
            [
                "id" => "2",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ],
            [
                "id" => "3",
                "command" => "Add",
                "operation" => "1 + 1",
                "result" => 2,
                "last_updated" => "2021-09-12 21:26:10"
            ]
        ];
        $this->insert($data, DriverEnum::composite());
    }
}
