<?php

namespace Jakmall\Recruitment\Calculator\Tests\Feature\Commands;

use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Tests\BaseTestCase;
use Jakmall\Recruitment\Calculator\Tests\Helpers\AppTrait;
use Symfony\Component\Console\Tester\CommandTester;

class SameCalculationTest extends BaseTestCase
{
    protected $commandTester;

    use AppTrait;

    public function setUp(): void
    {
        parent::setUp();
        $commands = $this->makeCommands()->find('add');
        $this->commandTester = new CommandTester($commands);
    }

    public function testSameAddCalculation()
    {
        $this->commandTester->execute(
            [
                'numbers' => [1,2,3]
            ]
        );

        $this->commandTester->execute(
            [
                'numbers' => [1,2,3]
            ]
        );

        $data = $this->read(DriverEnum::latest());
        $this->assertEquals(1, count($data));
    }
}
