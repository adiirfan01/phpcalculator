<?php

namespace Jakmall\Recruitment\Calculator\Repositories;

use Carbon\Carbon;

class LatestRepository
{
    protected $file;

    public function __construct()
    {
        $this->file = __DIR__."/../../storage/".getenv('LATEST_STORAGE');
    }

    /**
     * Save Data to latest.log
     *
     * @param array $data
     *
     * @return bool
     */
    public function save(array $data) : bool
    {
        $currentData = $this->getData();
        if($this->replaceIfExists($data, $currentData)) {
            return true;
        }
        $currentData = $this->removeOldestData($currentData);
        array_push($currentData, $data);
        $orderedData = $this->orderDataByNewest($currentData);
        $this->saveDataToFile($orderedData);
        return true;
    }

    /**
     * Order Data by updated_date
     *
     * @param array $data
     *
     * @return array
     */
    private function orderDataByNewest(array $data) : array
    {
        usort(
            $data, function ($a, $b) {
                return $b['last_updated'] <=> $a['last_updated'];
            }
        );

        return $data;
    }

    /**
     * Remove Oldest Data
     *
     * @param array $data
     *
     * @return array
     */
    private function removeOldestData(array $data) : array
    {
        $length = count($data);
        if($length == 10) {
            unset($data[$length-1]);
        }

        return $data;
    }

    /**
     * Find Data By id's
     *
     * @param $id
     *
     * @return array
     */
    public function findById($id) : array
    {
        try {
            $data = file_get_contents($this->file);
            $data = json_decode($data, true);
            $collection = collect($data);
            $result = $collection->whereIn('id', $id)
                ->map(
                    function ($item) {
                        return collect($item)
                        ->only(['id', 'command', 'operation', 'result']);
                    }
                )
                ->values()
                ->toArray();

            if($result) {
                $this->updateDataLastUpdated($id, $data);
                return $result;
            }

            return $result;
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * Find All Data
     *
     * @return array
     */
    public function findAll() : array
    {
        $data = file_get_contents($this->file);
        $data = json_decode($data, true);
        $collection = collect($data);
        $result = $collection->map(
            function ($item) {
                return collect($item)
                ->only(['id', 'command', 'operation', 'result']);
            }
        )->values()->toArray();

        return $result;
    }

    /**
     * Update Data last updated after read
     *
     * @param array $ids
     * @param array $data
     *
     * @throws \Exception
     */
    private function updateDataLastUpdated(array $ids, array $data)
    {
        foreach($ids as $id){
            $key = array_search($id, array_column($data, 'id'));
            if(false !== $key) {
                $data[$key]['last_updated'] = Carbon::now()->toDateTimeString();
            }
        }
        $orderedData = $this->orderDataByNewest($data);
        $this->saveDataToFile($orderedData);
    }

    /**
     * Save Data to file
     *
     * @param array $newData
     *
     * @throws \Exception
     */
    private function saveDataToFile(array $newData)
    {
        try {
            $newData = json_encode($newData, JSON_PRETTY_PRINT);
            file_put_contents($this->file, $newData);
        }catch(\Exception $e){
            throw new \Exception('Error When saving to File');
        }
    }

    /**
     * read data from log file
     *
     * @return array
     */
    private function getData() : array
    {
        $data = file_get_contents($this->file);
        $data = json_decode($data, true);

        return $data;
    }

    /**
     * Delete Data by id or clear everything
     *
     * @param  bool|array $ids
     * @return bool
     *
     * @throws \Exception
     */
    public function delete($ids = false)
    {
        $data = $this->getData();
        if($ids) {
            foreach($ids as $id){
                $key = array_search($id, array_column($data, 'id'));
                if(false !== $key) {
                    unset($data[$key]);
                }
            }
            $this->saveDataToFile(array_values($data));
            return true;
        }

        $this->saveDataToFile([]);
        return true;
    }

    /**
     * Update data id and last_updated if exists
     *
     * @param array $data
     * @param array $currents
     *
     * @return bool
     * @throws \Exception
     */
    private function replaceIfExists(array $data, array $currents) : bool
    {
        $key = array_search($data['operation'], array_column($currents, 'operation'));

        if(false !== $key) {
            $currents[$key]['id'] = $data['id'];
            $currents[$key]['last_updated'] = Carbon::now()->toDateTimeString();
            $orderedData = $this->orderDataByNewest($currents);
            $this->saveDataToFile($orderedData);
            return true;
        }

        return false;
    }
}
