<?php

namespace Jakmall\Recruitment\Calculator\Repositories;

use Carbon\Carbon;

class FileRepository
{
    protected $file;

    public function __construct()
    {
        $this->file = __DIR__."/../../storage/".getenv('FILE_STORAGE');
    }

    /**
     * Save Function
     *
     * @param array $data
     *
     * @return bool
     */
    public function save(array $data) : bool
    {
        $currentData = $this->getData();

        array_push($currentData, $data);
        $this->saveDataToFile($currentData);
        return true;
    }

    /**
     * Find Data by Id
     *
     * @param $id
     *
     * @return array
     */
    public function findById($id) : array
    {
        try {
            $data = $this->getData();
            $collection = collect($data);
            $result = $collection->whereIn('id', $id)
                ->map(
                    function ($item) {
                        return collect($item)
                        ->only(['id', 'command', 'operation', 'result']);
                    }
                )
                ->values()
                ->toArray();

            if($result) {
                return $result;
            }

            return $result;
        } catch (\Exception $e) {
            return [];
        }
    }

    public function findAll() : array
    {
        $data = $this->getData();
        $collection = collect($data);
        $result = $collection->sortByDesc('last_updated')
            ->map(
                function ($item) {
                    return collect($item)
                    ->only(['id', 'command', 'operation', 'result']);
                }
            )
            ->values()
            ->toArray();

        return $result;
    }

    /**
     * Save Data to file
     *
     * @param array $newData
     *
     * @throws \Exception
     */
    private function saveDataToFile(array $newData)
    {
        try {
            $newData = json_encode($newData, JSON_PRETTY_PRINT);
            file_put_contents($this->file, $newData);
        }catch(\Exception $e){
            throw new \Exception('Error When saving to File');
        }
    }

    /**
     * read data from log file
     *
     * @return array
     */
    private function getData() : array
    {
        $data = file_get_contents($this->file);
        $data = json_decode($data, true);

        return $data;
    }

    /**
     * Delete Data by id or clear everything
     *
     * @param  bool|array $ids
     * @return bool
     *
     * @throws \Exception
     */
    public function delete($ids = false)
    {
        $data = $this->getData();
        if($ids) {
            foreach($ids as $id){
                $key = array_search($id, array_column($data, 'id'));
                if(false !== $key) {
                    unset($data[$key]);
                }
            }
            $this->saveDataToFile(array_values($data));
            return true;
        }

        $this->saveDataToFile([]);
        return true;
    }
}
