<?php

namespace Jakmall\Recruitment\Calculator\History;

use Illuminate\Contracts\Container\Container;
use Jakmall\Recruitment\Calculator\Container\ContainerServiceProviderInterface;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\Services\StorageService;
use Jakmall\Recruitment\Calculator\Repositories\FileRepository;
use Jakmall\Recruitment\Calculator\Repositories\LatestRepository;
use Jakmall\Recruitment\Calculator\Services\CommandHistoryStorageService;

class CommandHistoryServiceProvider implements ContainerServiceProviderInterface
{
    /**
     * @inheritDoc
     */
    public function register(Container $container): void
    {
        $container->bind(
            StorageService::class,
            function ($container) {
                return new StorageService(
                    $container->make(FileRepository::class),
                    $container->make(LatestRepository::class),
                );
            }
        );

        $container->bind(
            CommandHistoryManagerInterface::class,
            function ($container) {
                return new CommandHistoryStorageService(
                    $container->make(StorageService::class)
                );
            }
        );

    }
}
