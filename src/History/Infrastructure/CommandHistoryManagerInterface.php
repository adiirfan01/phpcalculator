<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

//TODO: create implementation.
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;

interface CommandHistoryManagerInterface
{
    /**
     * Returns array of command history.
     *
     * @param DriverEnum $driver
     *
     * @return array returns an array of commands in storage
     */
    public function findAll(DriverEnum $driver): array;

    /**
     * Find a command by id.
     *
     * @param array      $id
     * @param DriverEnum $driver
     *
     * @return null|mixed returns null when id not found.
     */
    public function find($id, DriverEnum $driver): array;

    /**
     * Log command data to storage.
     *
     * @param mixed      $command The command to log.
     * @param DriverEnum $driver  Set Driver to save, default is composite
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($command, DriverEnum $driver): bool;

    /**
     * Clear a command by id
     *
     * @param array      $id
     * @param DriverEnum $driver
     *
     * @return bool Returns true when data with $id is cleared successfully, false otherwise.
     */
    public function clear($id, DriverEnum $driver): bool;

    /**
     * Clear all data from storage.
     *
     * @param DriverEnum $driver
     *
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clearAll(DriverEnum $driver):bool;
}
