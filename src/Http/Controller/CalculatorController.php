<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Enum\CalculationEnum;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Helpers\CalculationHelpers;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;


class CalculatorController
{

    protected $calculationHelpers;

    public function __construct(
        CommandHistoryManagerInterface $history,
        CalculationHelpers $calculationHelpers
    ) {
        $this->history = $history;
        $this->calculationHelpers = $calculationHelpers;
    }

    public function calculate(Request $request, $calculation)
    {
        try {
            $inputs = $request->input('input');
            if(!is_array($inputs)) {
                throw new \Exception('input is invalid');
            }

            $operation = $this->calculationHelpers->generateOperation(CalculationEnum::make($calculation), $inputs);
            $result = $this->calculationHelpers->calculateAll(CalculationEnum::make($calculation), $inputs);

            $this->history->log(
                [
                    'command' => $calculation,
                    'operation' => $operation,
                    'result' => $result,
                    'input' => $inputs,
                ], DriverEnum::composite()
            );

            return JsonResponse::create(
                [
                'command' => $calculation,
                'operation' => $operation,
                'result' => $result
                ], 201
            );

        }catch(\Throwable $th){
            return JsonResponse::create(
                [
                'message' => $th->getMessage()
                ], 400
            );
        }
    }
}
