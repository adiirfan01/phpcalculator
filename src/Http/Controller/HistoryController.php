<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Enum\CalculationEnum;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryController
{
    public function __construct(
        CommandHistoryManagerInterface $history
    ) {
        $this->history = $history;
    }

    public function index(Request $request)
    {
        try{
            if(!$request->query('driver')) {
                $driver = 'composite';
            }else{
                $driver = $request->query('driver');
            }
            $data = $this->history->findAll(DriverEnum::make($driver));
            $data = $this->getInputData($data);
            if(!$data) {
                return JsonResponse::create(
                    [
                    'message' => 'History Not Found'
                    ], 404
                );
            }
            return JsonResponse::create($data, 200);
        } catch(\Throwable $th){
            return JsonResponse::create(
                [
                'message' => $th->getMessage()
                ], 400
            );
        }
    }

    public function show(Request $request, $id)
    {
        try{
            if(!$request->query('driver')) {
                $driver = 'composite';
            }else{
                $driver = $request->query('driver');
            }
            $data = $this->history->find([$id], DriverEnum::make($driver));
            $data = $this->getInputData($data);
            if(!$data) {
                return JsonResponse::create(
                    [
                    'message' => 'History Not Found'
                    ], 404
                );
            }
            return JsonResponse::create($data[0], 200);
        } catch(\Throwable $th){
            return JsonResponse::create(
                [
                'message' => $th->getMessage()
                ], 400
            );
        }
    }

    public function remove($id, Request $request)
    {
        try{
            if(!$request->query('driver')) {
                $driver = 'composite';
            }else{
                $driver = $request->query('driver');
            }
            $this->history->clear([$id], DriverEnum::make($driver));
            return JsonResponse::create([], 204);
        } catch(\Throwable $th){
            return JsonResponse::create(
                [
                'message' => $th->getMessage()
                ], 400
            );
        }
    }

    private function getInputData(array $data)
    {
        foreach($data as $key => $calculation){
            $command = CalculationEnum::make($calculation['command'])->getValue();
            $data[$key]['input'] = array_map('intval', explode(" ".$command." ", $calculation['operation']));
        }

        return $data;
    }
}
