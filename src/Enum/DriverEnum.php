<?php

namespace Jakmall\Recruitment\Calculator\Enum;

use Spatie\Enum\Enum;

/**
 * @method static self composite()
 * @method static self file()
 * @method static self latest()
 */
class DriverEnum extends Enum
{
}
