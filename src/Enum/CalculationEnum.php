<?php

namespace Jakmall\Recruitment\Calculator\Enum;

use Spatie\Enum\Enum;

/**
 * @method static self add()
 * @method static self substract()
 * @method static self divide()
 * @method static self multiply()
 * @method static self power()
 */
class CalculationEnum extends Enum
{
    const MAP_VALUE = [
        'add' => '+',
        'substract' => '-',
        'divide' => '/',
        'multiply' => '*',
        'power' => '^'
    ];
}
