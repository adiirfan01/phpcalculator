<?php

namespace Jakmall\Recruitment\Calculator\Helpers;

use Jakmall\Recruitment\Calculator\Enum\CalculationEnum;

class CalculationHelpers
{
    /**
     * @param CalculationEnum $calculation
     * @param $inputs
     *
     * @return string
     */
    public function generateOperation(CalculationEnum $calculation, $inputs): string
    {
        $glue = sprintf(' %s ', $calculation->getValue());
        return implode($glue, $inputs);
    }

    /**
     * @param CalculationEnum $calculation
     * @param $inputs
     *
     * @return float|int|mixed|null
     */
    public function calculateAll(CalculationEnum $calculation, $inputs)
    {
        $number = array_pop($inputs);
        if (empty($inputs)) {
            return $number;
        }
        return $this->calculation($this->calculateAll($calculation, $inputs), $number, $calculation);
    }

    /**
     * @param $number1
     * @param $number2
     * @param CalculationEnum $calculation
     *
     * @return float|int|mixed|object
     */
    protected function calculation($number1, $number2, CalculationEnum $calculation)
    {
        $result = 0;
        switch ($calculation) {
        case CalculationEnum::add():
            $result = $number1 + $number2;
            break;
        case CalculationEnum::substract():
            $result = $number1 - $number2;
            break;
        case CalculationEnum::multiply():
            $result = $number1 * $number2;
            break;
        case CalculationEnum::divide():
            if($number2 == 0) {
                throw new \Exception('Error Division by Zero');
            }
            $result = $number1 / $number2;
            break;
        case CalculationEnum::power():
            $result = pow($number1, $number2);
            break;
        }
        return $result;
    }
}
