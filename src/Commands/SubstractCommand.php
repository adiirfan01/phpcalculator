<?php

namespace Jakmall\Recruitment\Calculator\Commands;

class SubstractCommand extends BaseCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'substract';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'substracted';

    /**
     * @var string
     */
    protected $operator = '-';
}
