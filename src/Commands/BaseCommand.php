<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Enum\CalculationEnum;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Helpers\CalculationHelpers;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class BaseCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    protected $calculationHelpers;

    public function __construct(
        CalculationHelpers $calculationHelpers
    ) {
        $this->signature = $this->generateSignature();
        $this->description = $this->generateDescription();
        $this->calculationHelpers = $calculationHelpers;
        parent::__construct();
    }

    public function handle(CommandHistoryManagerInterface $history): void
    {
        $numbers = $this->getInputs();
        $this->checkInput($numbers);
        $description = $this->calculationHelpers->generateOperation(
            CalculationEnum::make($this->commandVerb),
            $numbers
        );
        $result = $this->calculationHelpers->calculateAll(
            CalculationEnum::make($this->commandVerb),
            $numbers
        );
        $driver = $this->option('driver');

        $history->log(
            [
                'command' => ucfirst($this->commandVerb),
                'operation' => $description,
                'result' => $result,
            ], DriverEnum::make($driver)
        );

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    /**
     * @return array|string|null
     */
    protected function getInputs()
    {
        return $this->argument('numbers');
    }

    /**
     * @return string
     */
    protected function generateSignature(): string
    {
        return sprintf(
            '%s {numbers* : The numbers to be %s} {--d|driver=composite : %s}',
            $this->commandVerb,
            $this->commandPassiveVerb,
            'driver for storage (file, latest, composite)'
        );
    }

    /**
     * @return string
     */
    protected function generateDescription(): string
    {
        return sprintf(
            '%s all given Numbers',
            ucfirst($this->commandVerb)
        );
    }

    /**
     * @param array $numbers
     *
     * @throws \Exception
     */
    protected function checkInput(array $numbers)
    {
        foreach($numbers as $number){
            if(!is_numeric($number)) {
                throw new \Exception('this input "'. $number . '" is not number');
            }
        }
    }
}
