<?php

namespace Jakmall\Recruitment\Calculator\Commands;

class DivideCommand extends BaseCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'divide';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'divided';

    /**
     * @var string
     */
    protected $operator = '/';
}
