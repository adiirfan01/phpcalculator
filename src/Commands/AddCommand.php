<?php

namespace Jakmall\Recruitment\Calculator\Commands;

class AddCommand extends BaseCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'add';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'added';

    /**
     * @var string
     */
    protected $operator = '+';
}
