<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description = "Show Calculation History from log";

    public function __construct()
    {
        $commandVerb = "history:list";
        $filterIdDesc = "select data by id";
        $driverOptionDesc = "driver for storage (file, latest, composite)";

        $this->signature = sprintf(
            '%s {id?* : %s} {--d|driver=composite : %s}',
            $commandVerb, $filterIdDesc, $driverOptionDesc
        );

        parent::__construct();
    }

    public function handle(CommandHistoryManagerInterface $history): void
    {
        $id = $this->argument('id');
        $driver = $this->option('driver');

        ($id) ?
            $data = $history->find($id, DriverEnum::make($driver)) :
            $data = $history->findAll(DriverEnum::make($driver));

        if(!empty($data)) {
            $headers = ['id', 'Command', 'Operation', 'Result'];
            $this->table($headers, $data);
        } else {
            $this->comment("History is empty");
        }
    }
}
