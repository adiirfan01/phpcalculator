<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description = "Clear saved history";

    public function __construct()
    {
        $commandVerb = "history:clear";
        $filterIdDesc = "select id want to be removed";
        $driverOptionDesc = "driver for storage (file, latest, composite)";

        $this->signature = sprintf(
            '%s {id?* : %s} {--d|driver=composite : %s}',
            $commandVerb, $filterIdDesc, $driverOptionDesc
        );

        parent::__construct();
    }

    public function handle(CommandHistoryManagerInterface $history): void
    {
        $id = $this->argument('id');
        $driver = $this->option('driver');

        if($id) {
            $history->clear($id, DriverEnum::make($driver));
            $this->comment(
                sprintf(
                    "History with Id %s is removed",
                    implode(', ', $id),
                )
            );
        } else {
            $history->clearAll(DriverEnum::make($driver));
            $this->comment("All history is cleared");
        }
    }
}
