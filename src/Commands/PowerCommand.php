<?php


namespace Jakmall\Recruitment\Calculator\Commands;


class PowerCommand extends BaseCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'power';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'exponent';

    /**
     * @var string
     */
    protected $operator = '^';

    /**
     * @return array
     */
    protected function getInputs() : array
    {
        return [$this->argument('base'), $this->argument('exponent')];
    }

    /**
     * @return string
     */
    protected function generateSignature(): string
    {
        return sprintf(
            '%s {base : base number} {exponent : exponent number} {--d|driver=composite : %s}',
            $this->commandVerb,
            'driver for storage (file, latest, composite)'
        );
    }

    /**
     * @return string
     */
    protected function generateDescription(): string
    {
        return sprintf(
            '%s the given number',
            ucfirst($this->commandPassiveVerb)
        );
    }
}
