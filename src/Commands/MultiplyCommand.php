<?php

namespace Jakmall\Recruitment\Calculator\Commands;

class MultiplyCommand extends BaseCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'multiply';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'multiplied';

    /**
     * @var string
     */
    protected $operator = '*';
}
