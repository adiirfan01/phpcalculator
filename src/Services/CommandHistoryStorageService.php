<?php

namespace Jakmall\Recruitment\Calculator\Services;

use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CommandHistoryStorageService implements CommandHistoryManagerInterface
{

    protected $storageService;

    public function __construct(
        StorageService $storageServices
    ) {
        $this->storageService = $storageServices;
    }

    public function findAll(DriverEnum $driver): array
    {
        return $this->storageService->find($driver);
    }

    public function find($id, DriverEnum $driver): array
    {
        return $this->storageService->findByIds($id, $driver);
    }

    public function clear($id, DriverEnum $driver): bool
    {
        return (bool)($this->storageService->delete($id, $driver));
    }

    public function clearAll(DriverEnum $driver): bool
    {
        return (bool)($this->storageService->delete(false, $driver));
    }

    public function log($command, DriverEnum $driver): bool
    {
        return (bool)($this->storageService->save($command, $driver));
    }
}
