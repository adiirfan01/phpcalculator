<?php

namespace Jakmall\Recruitment\Calculator\Services;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Jakmall\Recruitment\Calculator\Enum\DriverEnum;
use Jakmall\Recruitment\Calculator\Repositories\FileRepository;
use Jakmall\Recruitment\Calculator\Repositories\LatestRepository;

class StorageService
{
    protected $fileRepo;

    protected $latestRepo;

    public function __construct(
        FileRepository $fileRepo,
        LatestRepository $latestRepo
    ) {
        $this->fileRepo = $fileRepo;
        $this->latestRepo = $latestRepo;
    }
    /**
     * Save Data to both file and latest
     *
     * @param array $data
     *
     * @return bool
     */
    public function save(array $data, DriverEnum $driver) : bool
    {
        $data = ['id' => Str::orderedUuid()] + $data;
        $data['last_updated'] = Carbon::now()->toDateTimeString();

        if(DriverEnum::file()->isEqual($driver)) {
            $this->fileRepo->save($data);
            return true;
        }

        if(DriverEnum::latest()->isEqual($driver)) {
            $this->latestRepo->save($data);
            return true;
        }

        $this->latestRepo->save($data);
        $this->fileRepo->save($data);
        return true;
    }

    /**
     * Find Data by id's
     *
     * @param $ids
     * @param DriverEnum $driver
     *
     * @return array
     */
    public function findByIds($ids, DriverEnum $driver) : array
    {
        if(DriverEnum::file()->isEqual($driver)) {
            $data = $this->fileRepo->findById($ids);
            return $data;
        }

        if(DriverEnum::latest()->isEqual($driver)) {
            $data = $this->latestRepo->findById($ids);
            return $data;
        }

        $data = $this->latestRepo->findById($ids);
        if(!$data) {
            $data = $this->fileRepo->findById($ids);
        }

        return $data;
    }

    /**
     * Find All Data
     *
     * @param DriverEnum $driver
     *
     * @return array
     */
    public function find(DriverEnum $driver) : array
    {
        if(DriverEnum::file()->isEqual($driver)) {
            $data = $this->fileRepo->findAll();
            return $data;
        }

        if(DriverEnum::latest()->isEqual($driver)) {
            $data = $this->latestRepo->findAll();
            return $data;
        }

        $data = $this->fileRepo->findAll();

        return $data;
    }

    /**
     * @param $id
     * @param DriverEnum $driver
     *
     * @return bool
     */
    public function delete($id, DriverEnum $driver) : bool
    {
        if(DriverEnum::file()->isEqual($driver)) {
            $this->fileRepo->delete($id);
            return true;
        }

        if(DriverEnum::latest()->isEqual($driver)) {
            $this->latestRepo->delete($id);
            return true;
        }

        $this->fileRepo->delete($id);
        $this->latestRepo->delete($id);

        return true;
    }
}
